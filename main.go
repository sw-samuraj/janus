// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/pprof"
	"time"

	"bitbucket.org/fifonaci/janus/janus"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

// Version is service version
var Version string

// BuildDate is date when service was built
var BuildDate string

// GitHash is has of GIT head for build
var GitHash string

func main() {
	var cfg janus.Config
	// Read pflags
	listenAddr := flag.String("listenAddr", ":5000", "Listen address, default :5000")
	configFile := flag.String("configFile", "config.yml", "Config file, default config.yml")
	mock := flag.Bool("mock", false, "Mockup Notification, default false")
	flag.Parse()

	janus.ReadFile(*configFile, &cfg)

	// logger
	var logger *zap.Logger
	var err error
	if cfg.Logging.Profile == "dev" {
		logger, err = zap.NewDevelopment()
		if err != nil {
			log.Fatal("Can't initilize logger ", err)
		}
	} else if cfg.Logging.Profile == "prod" {
		logger, err = zap.NewProduction()
		if err != nil {
			log.Fatal("Can't initilize logger ", err)
		}
	} else {
		log.Fatalf("Wrong Logging option %s ", cfg.Logging.Profile)
	}

	zap.ReplaceGlobals(logger)
	defer logger.Sync() // flushes buffer, if any

	janus.LInfo("Starting App",
		"Version", Version,
		"BuildDate", BuildDate,
		"GitHash", GitHash)

	var nClient janus.Notification
	if *mock {
		nClient = &janus.MockNotification{}
	} else {
		if cfg.SlackNotification.Enabled {
			nClient, err = janus.NewNotificationClient(cfg.SlackNotification.WebHookURL, cfg.SlackNotification.Channel)
			if err != nil {
				janus.LPanic("Failed to create Notification client",
					"err", err)
			}
		}
	}

	ex, err := janus.NewExecutor()
	if err != nil {
		janus.LPanic("Executor init failed")
	}

	var dClient janus.DockerClientI
	if cfg.Tests.StopUpgrades {
		dClient, err = janus.NewDockerClient(cfg.Tests.DockerHost)
		if err != nil {
			janus.LPanic("Failed to init docker client", "err", err)
		}
	}

	env := janus.Env{nClient, ex, cfg, dClient}

	mux := http.NewServeMux()
	mux.HandleFunc(janus.APIPath, env.EventCreateHandler)
	mux.HandleFunc(janus.PingPath, janus.PingHandler)

	// Register pprof handlers
	// access as http://localhost:5000/debug/pprof/
	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.HandleFunc("/debug/pprof/trace", pprof.Trace)

	// Prometheus
	mux.Handle("/metrics", promhttp.Handler())

	// Read Timeouts guide
	// https://ieftimov.com/post/make-resilient-golang-net-http-servers-using-timeouts-deadlines-context-cancellation/
	janus.LInfo(fmt.Sprintf("Starting server %v", *listenAddr))
	srv := &http.Server{
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 40 * time.Second,
		Addr:         *listenAddr,
		Handler: RequestIDMiddleware(loggingMw(http.TimeoutHandler(mux, janus.Timeout*time.Second,
			"Request Timeouted \n"))),
	}
	err = srv.ListenAndServe()
	log.Fatal(err)
}

// Logging middleware
func loggingMw(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t1 := time.Now().UnixNano()
		defer func() {
			janus.LRequest(
				"path", r.URL.Path,
				"remote", r.RemoteAddr,
				string(janus.ContextRequestID), r.Context().Value(janus.ContextRequestID),
				"lat", time.Now().UnixNano()-t1)
		}()
		next.ServeHTTP(w, r)
	})
}

// RequestIDMiddleware is middleware that generates request ID
func RequestIDMiddleware(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var requestID string
		if requestID = r.Header.Get(janus.RequestIDHTTPHeader); requestID == "" {
			requestID = uuid.New().String()
		}
		ctx := context.WithValue(r.Context(), janus.ContextRequestID, requestID)
		h.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
