// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExecutor(t *testing.T) {
	ex, err := NewExecutor()
	if err != nil {
		t.Fatal("Executor initialization failed.")
	}
	env := Env{Ex: ex}
	os := runtime.GOOS
	var cmd string
	if os == "Windows" {
		cmd = "dir"
	} else {
		cmd = "ls"
	}
	rc, stderr := env.Ex.Run(cmd)
	assert.Equal(t, 0, rc, "Returned incorrect rc")
	assert.Empty(t, stderr, "Stderr is not empty")
}

func TestExecutorNegativ(t *testing.T) {
	ex, err := NewExecutor()
	if err != nil {
		t.Fatal("Executor initialization failed.")
	}
	env := Env{Ex: ex}
	rc, stderr := env.Ex.Run("foo")
	assert.Equal(t, 1, rc, "Returned incorrect rc")
	assert.Equal(t, "exec: \"foo\": executable file not found in $PATH", stderr, "Expected non empty stderr")
}
