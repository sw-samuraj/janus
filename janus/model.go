// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"bufio"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const (
	// UpdateContainersTitle is used for update containers event
	UpdateContainersTitle = "Ouroboros has updated containers!"
	// HostsKey is key for host
	HostsKey = "Host/Socket:"
	// UpdatedContainersKey is key for updated
	UpdatedContainersKey = "Containers updated this pass:"
)

// Env structure represents env. configurations: database connectsions, ...
type Env struct {
	N   Notification  // Notification client
	Ex  Executor      // Tests executor
	Cfg Config        // Config
	D   DockerClientI // Docker client
}

// UpdatedContainers is list containers for tests execution
type UpdatedContainers struct {
	Socket                 string
	UpdatedContainersCount int
	Containers             []Container
}

// Container represents updated container
type Container struct {
	Name string
	From string
	To   string
}

// ParseContainers returns `UpdatedContainers`
func ParseContainers(msg string) (UpdatedContainers, error) {
	var containers UpdatedContainers
	if msg == "" {
		LMissingArgument("msg", msg)
		return containers, fmt.Errorf("Invalid argument")
	}
	lines := SplitLines(msg)
	var updatedConts int
	for _, line := range lines {
		if strings.HasPrefix(line, HostsKey) {
			match := findSubstring("Host/Socket:(.*)", line)
			containers.Socket = strings.TrimSpace(match[1])
		}
		if strings.HasPrefix(line, UpdatedContainersKey) {
			match := findSubstring("Containers updated this pass:(.*[0-9]+)", line)[1]
			n, err := strconv.Atoi(strings.TrimSpace(match))
			if err != nil {
				LError("Parsing containers count error", "line", line)
			}
			updatedConts = n
			containers.UpdatedContainersCount = updatedConts
		}
	}
	if updatedConts > 0 { // Parse data about updated containers
		for i := len(lines) - 1; i >= len(lines)-updatedConts; i-- {
			match := findSubstring("(.*) updated from (.*) to (.*)", lines[i])
			c := Container{
				Name: match[1],
				From: match[2],
				To:   match[3],
			}
			containers.Containers = append(containers.Containers, c)

		}
	}
	return containers, nil
}

func findSubstring(regex string, str string) []string {
	re := regexp.MustCompile(regex)
	return re.FindStringSubmatch(str)
}

// SplitLines returns strings splitted by lines
func SplitLines(s string) []string {
	var lines []string
	if s == "" {
		return lines
	}
	sc := bufio.NewScanner(strings.NewReader(s))
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	return lines
}
