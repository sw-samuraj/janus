// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"log"

	"go.uber.org/zap"
)

const (
	//LogRequestKey is key for logging requests
	LogRequestKey = "REQUEST"
	//MissingArgumentKey key for logging error for missing or incorrect  arguments
	MissingArgumentKey = "MISSINGARG"
)

var logger *zap.Logger

func init() {
	var err error
	logger, err = zap.NewDevelopment()
	if err != nil {
		log.Fatal("Can't initilize logger ", err)
	}
	zap.ReplaceGlobals(logger)
	defer logger.Sync() // flushes buffer, if any
}

//LInfo print info
func LInfo(msg string, keysAndValues ...interface{}) {
	if keysAndValues != nil {
		zap.S().Infow(msg, keysAndValues...)
	} else {
		zap.S().Infow(msg)
	}
}

//LRequest is used for http requests logging
func LRequest(keysAndValues ...interface{}) {
	if keysAndValues != nil {
		zap.S().Debugw(LogRequestKey, keysAndValues...)
	} else {
		zap.S().Debugw(LogRequestKey)
	}
}

//LMissingArgument shoudl be used for reporting missing arguments
func LMissingArgument(keysAndValues ...interface{}) {
	if keysAndValues != nil {
		zap.S().Errorw(MissingArgumentKey, keysAndValues...)
	} else {
		zap.S().Errorw(MissingArgumentKey)
	}
}

//LError print info
func LError(msg string, keysAndValues ...interface{}) {
	if keysAndValues != nil {
		zap.S().Errorw(msg, keysAndValues...)
	} else {
		zap.S().Errorw(msg)
	}
}

//LDebug print info
func LDebug(msg string, keysAndValues ...interface{}) {
	if keysAndValues != nil {
		zap.S().Debugw(msg, keysAndValues...)
	} else {
		zap.S().Debugw(msg)
	}
}

//LPanic print info
func LPanic(msg string, keysAndValues ...interface{}) {
	if keysAndValues != nil {
		zap.S().Panicw(msg, keysAndValues...)
	} else {
		zap.S().Panicw(msg)
	}
	panic(msg)
}
