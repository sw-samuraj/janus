// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

// Config is config file structure
type Config struct {
	Logging struct {
		Profile string `yaml:"profile"`
	} `yaml:"logging"`
	Tests struct {
		ExecPath     string `yaml:"exec_path"`
		StopUpgrades bool   `yaml:"suspend_upgrades"`
		DockerHost   string `yaml:"docker_host"`
	} `yaml:"tests"`
	SlackNotification struct {
		Enabled    bool   `yaml:"enabled"`
		WebHookURL string `yaml:"webhook_url"`
		Channel    string `yaml:"channel"`
	} `yaml:"slack_notification"`
}

func processError(err error) {
	fmt.Println(err)
	os.Exit(2)
}

// ReadFile reads config from file
//TODO: switch to io.Reader
func ReadFile(configFile string, cfg *Config) {
	f, err := os.Open(configFile)
	if err != nil {
		processError(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		processError(err)
	}
}
