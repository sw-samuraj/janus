// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"
)

type execMock struct {
	mock.Mock
}

func (m *execMock) Run(cmd string) (int, string) {
	LInfo("Exec Mockup", "cmd", cmd)
	args := m.Called(cmd)
	return args.Int(0), args.String(1)
}

type slackMock struct {
	mock.Mock
}

func (m *slackMock) SendMessage(ctx context.Context, mType MessageType, message string) error {
	m.Called()
	LInfo("Slack Mockup", "msg", message)
	return nil
}

func TestFailedTestsScenario(t *testing.T) {
	var ctx = context.Background()
	cmd := "date"
	execM := new(execMock)
	execM.On("Run", cmd).Return(42, "FailedTests")

	slackM := new(slackMock)
	slackM.On("SendMessage")

	cfg := Config{}
	cfg.Tests.ExecPath = cmd
	cfg.SlackNotification.Enabled = true
	env := Env{Ex: execM, N: slackM, Cfg: cfg}

	containers := UpdatedContainers{
		Socket:                 "Test",
		UpdatedContainersCount: 1,
		Containers: []Container{
			{Name: "resource-manager",
				From: "00a470f8e6",
				To:   "7a31ce12ac"},
		},
	}
	env.runTests(ctx, containers)
	slackM.AssertNumberOfCalls(t, "SendMessage", 2) //Test that we call slack at the beg and end
}
