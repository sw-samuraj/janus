# systemd example

* Create janus.service in /usr/lib/systemd/system
* Run `systemctl daemon-reload`
* Run `systemctl enable janus.service && systemctl start janus.service`